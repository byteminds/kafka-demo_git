﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Confluent.Kafka;
using Confluent.Kafka.Serialization;

namespace DemoApp.Kafka
{
    public class KafkaProducer : IDisposable
    {
        private readonly Producer<Null, string> _producer;

        public KafkaProducer(string brokerEndpoints)
        {

            var config = new Dictionary<string, object>
            { 
                    { "bootstrap.servers", brokerEndpoints },
                    { "api.version.request", true },
                    { "socket.blocking.max.ms", 1 },
                    { "queue.buffering.max.ms", 5 },
                //   { "broker.version.fallback", "0.8.2.1" },
                    { "queue.buffering.max.kbytes", 10240 },

                    { "debug", "msg" },
                    {
                        "default.topic.config",
                        new Dictionary<string, object>
                            {
                                { "message.timeout.ms", 3000 },
                                { "request.required.acks", 0 }
                            }
                    }
            };
            _producer = new Producer<Null, string>(config, new NullSerializer(), new StringSerializer(Encoding.UTF8));
            _producer.OnLog += OnLog;
            _producer.OnError += OnError;
        }

        public async Task<Message<Null, string>> ProduceAsync(string value, string topic, int partition = -1)
        {
            Message<Null, string> message = null;
            try
            {
                if (partition < 0)
                {
                    message = await _producer.ProduceAsync(topic, null, value);
                }
                else
                {
                    message = await _producer.ProduceAsync(topic, null, value, partition);
                }

                if (message.Error.HasError)
                {
                    throw new KafkaException(message.Error);
                }

                return message;
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    ex.ToString(),
                    "Error producing to Kafka. Topic/partition: '{0}/{1}'. Message: {2}'.",
                    topic,
                    partition,
                    message?.Value ?? "N/A");
                throw;
            }


        }

        public void Dispose()
        {
            if (_producer != null)
            {
                _producer.OnLog -= OnLog;
                _producer.OnError -= OnError;
                _producer.Dispose();
            }
        }

        private void OnLog(object sender, LogMessage logMessage)
            => Console.WriteLine(
                $"Producing to Kafka. Client: {logMessage.Name}, syslog level: '{logMessage.Level}', message: {logMessage.Message}."
             );

        private void OnError(object sender, Error error)
            => Console.WriteLine("Producer error: {0}. No action required.", error);
    }
}