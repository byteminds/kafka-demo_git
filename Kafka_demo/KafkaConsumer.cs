﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Confluent.Kafka;
using Confluent.Kafka.Serialization;


namespace DemoApp.Kafka
{
    public sealed class KafkaConsumer : IDisposable
    {
        public Consumer<Null, string> Kernel { get; }

        public KafkaConsumer(string brokerEndpoints, string groupId)
        {

            var config = new Dictionary<string, object>
                {
                    { "bootstrap.servers", brokerEndpoints },
                    { "api.version.request", true },
                    { "group.id", !string.IsNullOrEmpty(groupId) ? groupId : Guid.NewGuid().ToString() },
                    { "socket.blocking.max.ms", 1 },
                    { "enable.auto.commit", false },
                    { "fetch.wait.max.ms", 5 },
                    { "fetch.error.backoff.ms", 5 },
                    { "fetch.message.max.bytes", 10240 },
                    { "queued.min.messages", 1000 },
#if DEBUG
                  //  { "debug", "msg" },
#endif
                    {
                        "default.topic.config",
                        new Dictionary<string, object>
                            {
                                { "auto.offset.reset", "beginning" }
                            }
                    }
                };
            Kernel = new Consumer<Null, string>(config, new NullDeserializer(), new StringDeserializer(Encoding.UTF8));
        }


        public Task Consume(CancellationToken cancellationToken, IEnumerable<string> topics)
        {
            return Task.Run(() =>
             {

                 try
                 {
                     Kernel.Subscribe(topics);
                     while (!cancellationToken.IsCancellationRequested)
                     {
                         Kernel.Poll(TimeSpan.FromMilliseconds(100));
                     }

                 }
                 finally
                 {
                     Kernel.Unsubscribe();
                 }
             });

        }

        public async Task CommitAsync(Message<Null, string> message) => await Kernel.CommitAsync(message);

        public void Dispose()
        {
            if (Kernel != null)
            {
                //_consumer.OnLog -= OnLog;
                //_consumer.OnError -= OnError;
                //_consumer.OnConsumeError -= OnConsumeError;
                Kernel.Dispose();
            }
        }

        private void OnLog(object sender, LogMessage logMessage)
            => Console.WriteLine(
                "Consuming from Kafka. Client: '{0}', syslog level: '{1}', message: '{2}'.",
                logMessage.Name,
                logMessage.Level,
                logMessage.Message);

        private void OnError(object sender, Error error)
            => Console.WriteLine("Consumer error: {0}. No action required.", error);

        private void OnConsumeError(object sender, Message message)
        {
            Console.WriteLine(
                "Error consuming from Kafka. Topic/partition/offset: '{0}/{1}/{2}'. Error: '{3}'.",
                message.Topic,
                message.Partition,
                message.Offset,
                message.Error);
            throw new KafkaException(message.Error);
        }
    }

}