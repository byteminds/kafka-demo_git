﻿using Confluent.Kafka;
using System;
using System.Collections;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace DemoApp.Kafka
{


    public static partial class Program
    {
        private const string _kafkaNode1 = "192.168.0.102:9092";
        private const string _kafkaNode2 = "192.168.0.102:9093";


        public static void Main(string[] args)
        {
            //CheckPort();
            
            if (args[0] == "-p")
            {
                using (var producer = new KafkaProducer($"{_kafkaNode1},{_kafkaNode2}"))
                {
                    while (true)
                    {
                        var a = producer.ProduceAsync(Guid.NewGuid().ToString(), "demo").Result;
                    }

                }
            }
            else if (args[0] == "-c")
            {
                using (var consumer = new KafkaConsumer($"{_kafkaNode1},{_kafkaNode2}", "1"))
                {
                    var count = 0;
                    var tokenSource = new CancellationTokenSource();
                    var stopwatch = new Stopwatch();

                    consumer.Kernel.OnMessage += (sender, msg) =>
                    {
                        Console.WriteLine($"{msg.Topic}/{msg.Partition} @{msg.Offset}: '{msg.Value}'");
                        consumer.CommitAsync(msg).GetAwaiter().GetResult();
                        count++;
                    };

                    var task = consumer.Consume(tokenSource.Token, new[] { "demo" });
                    task.Wait();
                }
            }
            else if (args[0] == "-rx")
            {
                using (var demo = new RxConsumerDemo(_kafkaNode1, new[] { "demo" }, 1))
                {
                    var cts = new CancellationTokenSource();
                    demo.RunAsync(cts.Token).GetAwaiter().GetResult();
                }
            }
            else
            {
                Console.WriteLine("Support commands are -p, -c");
            }
        }

        private static void CheckPort()
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.ConnectAsync("192.168.0.102", 9092).GetAwaiter().GetResult();
                    Console.WriteLine("Port open");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Port closed");
                }
            }
        }

    }
}