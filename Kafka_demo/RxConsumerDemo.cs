﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace DemoApp.Kafka
{
    public sealed class RxConsumerDemo : IDisposable
    {
        private const string GroupId = "1";

        private readonly RxKafkaConsumer _kafkaConsumer;
        private readonly int _batchSize;

        private IDisposable _registration;

        public RxConsumerDemo(string brokerEndpoints, IEnumerable<string> topics, int batchSize)
        {
            _batchSize = batchSize;
            _kafkaConsumer = new RxKafkaConsumer(brokerEndpoints, GroupId, topics);
        }

        public async Task RunAsync(CancellationToken cancellationToken)
        {
            var observable = _kafkaConsumer.Consume(cancellationToken);

            var subscription = observable.Buffer(_batchSize)
                      .Subscribe(
                          messages =>
                          {
                              foreach (var message in messages)
                              {
                                  Console.WriteLine($"{message.Topic}/{message.Partition} @{message.Offset}: '{message.Value}'");
                              }

                              _kafkaConsumer.CommitAsync(messages[messages.Count - 1]).GetAwaiter().GetResult();
                          });

            var taskCompletionSource = new TaskCompletionSource<object>();
            _registration = cancellationToken.Register(
                () =>
                {
                    subscription.Dispose();
                    taskCompletionSource.SetResult(null);
                });

            await taskCompletionSource.Task;
        }

        public void Dispose()
        {
            _registration.Dispose();
            _kafkaConsumer.Dispose();
        }
    }

}